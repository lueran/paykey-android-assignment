package com.eranlunenfeld.transactionviewer.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.eranlunenfeld.transactionviewer.productdetails.TransactionsAdapter;

/**
 * Created by eranlunenfeld on 09/12/2017.
 */

public class Transaction implements Parcelable, TransactionsAdapter.ListItem
{
    private String sku;
    private String currency;
    private double amount;

    public Transaction(String sku, String currency, double amount)
    {
        this.sku = sku;
        this.currency = currency;
        this.amount = amount;
    }

    protected Transaction(Parcel in)
    {
        sku = in.readString();
        currency = in.readString();
        amount = in.readDouble();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>()
    {
        @Override
        public Transaction createFromParcel(Parcel in)
        {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size)
        {
            return new Transaction[size];
        }
    };

    public String getSku()
    {
        return sku;
    }

    public void setSku(String sku)
    {
        this.sku = sku;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(sku);
        parcel.writeString(currency);
        parcel.writeDouble(amount);
    }

    @Override
    public boolean isSection()
    {
        return false;
    }
}
