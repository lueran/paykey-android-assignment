package com.eranlunenfeld.transactionviewer.productdetails;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.eranlunenfeld.transactionviewer.R;
import com.eranlunenfeld.transactionviewer.models.Transaction;

import java.util.ArrayList;

public class ProductDetailsActivity extends AppCompatActivity
{
    public static final String TRANSACTIONS = "com.eranlunenfeld.transactionviewr.TRANSACTIONS";
    public static final String SKU = "com.eranlunenfeld.transactionviewr.SKU";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        Intent intent = getIntent();

        if (intent.hasExtra(SKU))
        {
            setTitle(getString(R.string.transaction_title_format, intent.getStringExtra(SKU)));
        }

        ArrayList<Transaction> transactions;

        if (intent.hasExtra(TRANSACTIONS))
        {
            transactions = intent.getParcelableArrayListExtra(TRANSACTIONS);
        }
        else
        {
            transactions = new ArrayList<>();
        }

        RecyclerView transactionsRecyclerView = findViewById(R.id.transactions_recyclerView);
        transactionsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        transactionsRecyclerView.setAdapter(new TransactionsAdapter(this, transactions));
    }
}
