package com.eranlunenfeld.transactionviewer.productdetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eranlunenfeld.transactionviewer.R;
import com.eranlunenfeld.transactionviewer.utils.RateConverterManager;
import com.eranlunenfeld.transactionviewer.models.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * Created by eranlunenfeld on 09/12/2017.
 */

public class TransactionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final int TRANSACTION_TYPE = 0;
    private static final int SECTION_TYPE = 1;
    private final Context context;

    private ArrayList<ListItem> dataSource = new ArrayList<>();

    public TransactionsAdapter(Context context, ArrayList<Transaction> transactions)
    {
        this.context = context;

        HashMap<String, List<Transaction>> transactinGroupCurrency = new HashMap<>();

        for (Transaction transaction : transactions)
        {
            List<Transaction> transactionsForCurrency = transactinGroupCurrency.get(transaction.getCurrency());

            if (transactionsForCurrency == null)
            {
                transactionsForCurrency = new ArrayList<>();
            }

            transactionsForCurrency.add(transaction);
            transactinGroupCurrency.put(transaction.getCurrency(), transactionsForCurrency);
        }

        for (String currency : transactinGroupCurrency.keySet())
        {
            dataSource.add(new SectionItem(currency));
            dataSource.addAll(transactinGroupCurrency.get(currency));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        switch (viewType)
        {
            case SECTION_TYPE:
            {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_transaction_section, parent, false);
                return new TransactionSectionViewHolder(view);
            }
            default:
            {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_transaction, parent, false);
                return new TransactionViewHolder(view);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        switch (getItemViewType(position))
        {
            case TRANSACTION_TYPE:
                ((TransactionViewHolder) holder).bind((Transaction) dataSource.get(position));
                break;
            case SECTION_TYPE:
                ((TransactionSectionViewHolder) holder).bind((SectionItem) dataSource.get(position));
                break;
        }

    }

    @Override
    public int getItemCount()
    {
        return dataSource.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        return dataSource.get(position).isSection() ? SECTION_TYPE : TRANSACTION_TYPE;
    }

    class TransactionViewHolder extends RecyclerView.ViewHolder
    {
        TextView amountTextView;
        TextView gbpAmountTextView;

        public TransactionViewHolder(View itemView)
        {
            super(itemView);
            amountTextView = itemView.findViewById(R.id.list_item_transaction_amount);
            gbpAmountTextView = itemView.findViewById(R.id.list_item_transaction_amount_gbp);
        }

        public void bind(Transaction transaction)
        {
            amountTextView.setText(String.format(Locale.US,
                    "%.2f %s",
                    transaction.getAmount(),
                    transaction.getCurrency()));

            gbpAmountTextView.setText(String.format(Locale.US,
                    "%.2f GBP",
                    "GBP".equals(transaction.getCurrency())
                            ? transaction.getAmount()
                            : RateConverterManager.getInstance(context).convertRate(transaction.getCurrency(), "GBP") * transaction.getAmount()));
        }
    }

    private class TransactionSectionViewHolder extends RecyclerView.ViewHolder
    {
        TextView sectionTitle;

        public TransactionSectionViewHolder(View view)
        {
            super(view);
            sectionTitle = view.findViewById(R.id.list_item_transaction_sectoin_title);
        }

        void bind(SectionItem sectionItem)
        {
            sectionTitle.setText(sectionItem.title);
        }
    }

    public interface ListItem
    {
        boolean isSection();
    }

    private class SectionItem implements ListItem
    {
        String title;

        public SectionItem(String title)
        {
            this.title = title;
        }

        @Override
        public boolean isSection()
        {
            return true;
        }
    }
}
