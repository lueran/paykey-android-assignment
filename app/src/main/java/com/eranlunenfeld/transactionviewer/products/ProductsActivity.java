package com.eranlunenfeld.transactionviewer.products;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.eranlunenfeld.transactionviewer.R;
import com.eranlunenfeld.transactionviewer.models.Transaction;
import com.eranlunenfeld.transactionviewer.productdetails.ProductDetailsActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductsActivity extends AppCompatActivity
{
    private RecyclerView productsRecyclerView;
    private ProductsAdapter productsAdapter;
    private ProductsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        productsRecyclerView = findViewById(R.id.products_recyclerView);

        initRecyclerView();
        presenter = new ProductsPresenter(this);
        HashMap<String, List<Transaction>> products = presenter.getProductTransactions();
        initData(products);
    }

    @Override
    protected void onDestroy()
    {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void initRecyclerView()
    {
        productsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        productsAdapter = new ProductsAdapter(this);
        productsAdapter.setListener(new ProductsAdapter.OnProductClickListener()
        {
            @Override
            public void onProductClick(Map.Entry<String, List<Transaction>> productTransaction)
            {
                Intent transactionsDetailsIntent = new Intent(ProductsActivity.this, ProductDetailsActivity.class);
                transactionsDetailsIntent.putExtra(ProductDetailsActivity.SKU, productTransaction.getKey());
                transactionsDetailsIntent.putParcelableArrayListExtra(ProductDetailsActivity.TRANSACTIONS,
                        new ArrayList<>(productTransaction.getValue()));
                startActivity(transactionsDetailsIntent);
            }
        });

        productsRecyclerView.setAdapter(productsAdapter);
    }

    private void initData(HashMap<String, List<Transaction>> products)
    {
        productsAdapter.setProductsTransactions(products);
    }
}
