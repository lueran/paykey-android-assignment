package com.eranlunenfeld.transactionviewer.products;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eranlunenfeld.transactionviewer.R;
import com.eranlunenfeld.transactionviewer.models.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by eranlunenfeld on 09/12/2017.
 */

class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>
{
    private Context context;
    private ArrayList<Map.Entry<String, List<Transaction>>> productsTransactions;
    private OnProductClickListener listener;

    public ProductsAdapter(Context context)
    {
        this.context = context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position)
    {
        holder.bind(productsTransactions.get(position));
    }

    @Override
    public int getItemCount()
    {
        return productsTransactions.size();
    }

    public void setListener(OnProductClickListener listener)
    {
        this.listener = listener;
    }

    public void setProductsTransactions(Map<String, List<Transaction>> productsTransactions)
    {
        this.productsTransactions = new ArrayList<>(productsTransactions.entrySet());
        notifyDataSetChanged();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView skuTextView;
        TextView transactionCountTextView;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            skuTextView = itemView.findViewById(R.id.list_item_product_sku);
            transactionCountTextView = itemView.findViewById(R.id.list_item_product_transaction_count);
        }

        public void bind(final Map.Entry<String, List<Transaction>> productTransaction)
        {
            skuTextView.setText(productTransaction.getKey());
            transactionCountTextView.setText(context.getResources().getQuantityString(R.plurals.transactions_count,
                    productTransaction.getValue().size(),
                    productTransaction.getValue().size()));

            itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if (listener != null)
                    {
                        listener.onProductClick(productTransaction);
                    }
                }
            });
        }
    }

    public interface OnProductClickListener {
        void onProductClick(Map.Entry<String, List<Transaction>> productTransaction);
    }
}
