package com.eranlunenfeld.transactionviewer.products;

import android.content.Context;

import com.eranlunenfeld.transactionviewer.models.Transaction;
import com.eranlunenfeld.transactionviewer.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by eranlunenfeld on 11/12/2017.
 */

public class ProductsPresenter
{
    private static final String TRANSACTIONS_ASSET_FILE_NAME = "transactions1.json";

    private Context context;

    public ProductsPresenter(Context context)
    {
        this.context = context;
    }

    HashMap<String, List<Transaction>> getProductTransactions()
    {
        HashMap<String, List<Transaction>> products = new HashMap<>();
        Transaction[] transactions = getTransactionsFromAssets(TRANSACTIONS_ASSET_FILE_NAME);

        for (Transaction transaction : transactions)
        {
            List<Transaction> productTransaction = products.get(transaction.getSku());

            if (productTransaction == null)
            {
                productTransaction = new ArrayList<>();
            }

            productTransaction.add(transaction);
            products.put(transaction.getSku(), productTransaction);
        }

        return products;
    }

    private Transaction[] getTransactionsFromAssets(String fileName)
    {
        Transaction[] transactions = new Gson().fromJson(Utils.loadJSONFromAsset(context, fileName), Transaction[].class);

        if (transactions == null)
        {
            transactions = new Transaction[0];
        }

        return transactions;
    }

    public void onDestroy()
    {
        context = null;
    }
}
