package com.eranlunenfeld.transactionviewer.utils;

import android.content.Context;

import com.eranlunenfeld.transactionviewer.models.Rate;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by eranlunenfeld on 09/12/2017.
 */

public class RateConverterManager
{
    private static final String RATES_ASSET_FILE_NAME = "rates1.json";
    private static RateConverterManager instance = null;

    private double[][] ratesConverter;
    private ArrayList<String> currencyArray;

    public static RateConverterManager getInstance(Context context)
    {
        if (instance == null)
        {
            instance = new RateConverterManager(context);
        }

        return instance;
    }

    private RateConverterManager(Context context)
    {
        Rate[] rates = getRatesFromAssets(context, RATES_ASSET_FILE_NAME);

        initConverterRates(rates);
    }

    private Rate[] getRatesFromAssets(Context context, String fileName)
    {
        Rate[] rates = new Gson().fromJson(Utils.loadJSONFromAsset(context, fileName), Rate[].class);

        if (rates == null)
        {
            rates = new Rate[0];
        }

        return rates;
    }

    private void initConverterRates(Rate[] rates)
    {
        // Initialize all currencies available
        HashSet<String> currency = new HashSet<>();

        for (Rate rate : rates)
        {
            currency.add(rate.getFrom());
            currency.add(rate.getTo());
        }

        currencyArray = new ArrayList<>(currency);

        ratesConverter = new double[currency.size()][currency.size()];

        // Initialize knows rates
        for (Rate rate : rates)
        {
            ratesConverter[currencyArray.indexOf(rate.getFrom())][currencyArray.indexOf(rate.getTo())] = rate.getRate();
        }

        /*
            Calculate all convert rates using change of Floyd–Warshall algorithm
         */
        for (int k = 0; k < ratesConverter[0].length; k++)
        {
            for (int x = 0; x < ratesConverter.length; x++)
            {
                for (int y = 0; y < ratesConverter[0].length; y++)
                {
                    if (ratesConverter[x][y] == 0)
                    {
                        ratesConverter[x][y] = ratesConverter[x][k] * ratesConverter[k][y];
                    }
                    else if ((ratesConverter[x][k] != 0) && (ratesConverter[k][y] != 0))
                    {
                        ratesConverter[x][y] = Math.min(ratesConverter[x][y], ratesConverter[x][k] * ratesConverter[k][y]);
                    }
                }
            }
        }
    }

    public double convertRate(String from, String to)
    {
        return ratesConverter[currencyArray.indexOf(from)][currencyArray.indexOf(to)];
    }
}
